const sqlite3 = require('sqlite3').verbose();
var fs = require('fs');
if (!fs.readFileSync('./config.ini')) { exit; }
var ini = require('ini');
const { exit } = require('process');
var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

var db = new sqlite3.Database(config.dbFileName);
var dbname = config.dbname;
var domain = config.domain;
var pass = config.pass;

//POST
var querystring = require('querystring');
var https = require('https');

// random number gen
var rand = (max, min) => { min = min || 0; return Math.floor(Math.random() * (max - min + 1) + min) };


main();


function main() {
  try {
    // Generate account and add account to DB
    db.all("SELECT * from " + dbname, (err, rows) => insertAcc(err,rows,db,1));
    // get data after
    //db.all("SELECT * FROM " + dbname, (err,row) => logData(err,row));
  } catch (err) {
    console.error(err);
    throw err;
  } 
}

function clearTableData(db,table) {
  db.run("DELETE FROM "+table);
}

function logData(err,rows) {
  if (err)
    console.error(err);
  console.log(rows,"Rows called");
}

// Insert account
function insertAcc(err,rows,db,iterations) {
  if (err) console.error(err);
  if (rows) console.log("we have this many rows: " + rows.length);
  db.serialize(function () {
    let stmt = db.prepare("INSERT INTO accounts(email,pass) VALUES (?,?)");
    for (var i = 0; i < iterations; i++) {
      var acc = new genAcc();
      console.log("iteration " + i + " out of " + iterations);

      stmt.run(acc.email, acc.pass)
      // MAKE ACCOUNT ON JAGEX SITE
      registerAccount(acc.email, acc.pass)
    }
    stmt.finalize();
  })
}

//Account generator
function genAcc(domain, accArray) {
  accArray=accArray!=undefined||[];
  // random character gen
  var words = (fs.readFileSync('./lib/words.txt', 'utf-8')).split("\r\n");
  var randString = () => { return Math.random().toString(36).slice(2) }

  this.email = words[rand(words.length, 1)] + words[rand(words.length, 1)] + rand(1, 999).toString() + "@" + domain;
  this.pass = randString();
  console.log("Generated",this.email,this.pass);
  return;
}

function registerAccount(email,pass) {
  const cookie = require('cookie');

  // Build the post string from an object
  var post_data = querystring.stringify({
    'email1': email,
    'password1': pass,
    'output_format': 'json',
    /*other misc data required by jagex*/
    'onlyOneEmail': "1",
    'onlyOnePassword': "1",
    'day': "1",
    'month': "1",
    'year': "1",
    'create-submit': "create",
    'theme': ['oldschool', 'oldschool']
  });

  // An object of options to indicate where to post to

  var cParsed = cookie.parse("settings=Ymi9Liy-EKCN6D3mDdihco3oPeYN2KFymqK81A5qs*c; JXWEBUID=A427F9A527E3258B152922AA4AF9333B2CB84F1944073DA4AF745AE3C17256976B79E1727A0E4717; global__user-visited=true; visid_incap_2325521=1rDQTXNYTC+YjOx8BSQGT3WzBV8AAAAAQUIPAAAAAACNhNe4WBEjBVtz44BkV7Aq; jagex_accept_cookies=true; JXDOB=19921118; slangpref=0; JXFLOWCONTROL=19420408981350372; PASS_CODE_648946000=1552793732; JXTRACKING=0152473C66000001732DD1DECD; JXADDINFO=DBXPZaBPotHnzeZldoHBTxXnbnMFe3VcAAAAAAA; nlbi_2325521=37J7cR1ESz/mDrVg06riGgAAAACJAOTdnDAzcWgZQbS7jsHZ; nlbi_2325521_2147483646=gtw6OsOcUk+opfst06riGgAAAACPnCoKGc/96nV8Fij15yll; JSESSIONID=7F140B453FF2E0CBDA4B7B22814BDF04; JSESSIONID=7B8C6B3BBE6655EF6EF78ED32C81E063; usrdob=-4440; incap_ses_1185_2325521=0mwHNJ5fqRK55KQEUPdxEHXOR18AAAAAQZB+9AuM0mOQ4fWfz0nhsw==; SJAGS=a2d0c7b1-5050-4bf0-934e-6772a21d112b; reese84=3:bIx7eRQeBOhjK/4RTx6NnQ==:moySqJkGEoENN8vw1mlgfuhiLRpXXa2HiCOZTrGBfRKtEIAbq+FpLmO3WCu2g49lPGN/b8/bCgPJTpQ9nb2YJ5k962+BaYmLGTUNO8BQI+v8z79l7BSVPliNiGh42s48BOaNMBkc0EvTdvd/bgyhIk0PNsgPbG0F850zsyn1MSGVKNcOou35t0YlY2ylm0kOtYtEU+obTT4TXfbSzByBjjPYm9MPJyaCsss/P3RbnTprxOZLYVlRlmhzD3rSngDUfZkMtT4OoBJxF3DhVdxGdXW4okDKUcROL8nk4GHeqe59DJVc1BQ+jj0PuYHDCOM97OC9w4Qux+51OHyqdH8TbZLnpWFAXS61IDkirEIbxVok4oMgrAl9xSVI8GvbIlTsnl9nUOYA2FNYiyeZ7fsRHQJOd3900ZmikLl3c1bUH7g=:EQXEUEm2QnIY0XgsAuf3niRQKzdpAnIudsqktdSs5Yk=; sameday=sameday");


  var post_options = {
    host: 'secure.runescape.com',
    port: '443',
    path: '/m=account-creation/create_account',
    method: 'POST',
    headers: {
      'ciphers': 'DES-CBC3-SHA',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Referer': 'https://oldschool.runescape.com/',
      'Accept': '*/*',
      'Accept-Encoding': 'gzip, deflate, br',
      'Accept-Language': 'en-US,en;q=0.5',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0',
      'Content-Length': Buffer.byteLength(post_data)
    }
  };

  //pageToken = "/af92d108180cb7c3523f6242ce5660c3"
  if (pageToken) post_options.path += pageToken;

  var cookieString='';
  Object.keys(cParsed).forEach((val) => {
    cookieString += val + ":" + cParsed[val] + "; ";
  })
  post_options.headers['Cookie'] = cookieString;

  console.log(post_options,"headers");
  // Set up the request
  var post_req = https.request(post_options, function (res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      console.log('Response: ' + chunk);
    });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();

  const puppeteer = require('puppeteer');

  (async () => {
    const browser = await puppeteer.launch({headless:false, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
    const page = await browser.newPage();
    await page.goto('http://' + post_options.host + post_options.path, { waitUntil: 'networkidle2' });

    // Get the "viewport" of the page, as reported by the page.
    const dimensions = await page.evaluate(() => {
      return {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight,
        deviceScaleFactor: window.devicePixelRatio
      };
    });

    console.log('Dimensions:', dimensions);
    await page.screenshot({ path: 'example.png' });
    //await browser.close();
  })();

}

function writeIni(key, value) {
  let config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'))
  config[key] = value;
  fs.writeFileSync('./config.ini', ini.stringify(config))
}

function endMe(db) {
	if (db.close()) console.log("DB closed");
}

